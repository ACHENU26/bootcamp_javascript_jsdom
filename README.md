# BOOTCAMP 2020
## BORDEAUX YNOV CAMPUS 
### Réaliser les 3 TP du cours 200 sur la manipulation du DOM avec JS, le rendu se fera via git.

# Exercice 1 
Récupérez (ou Forkez) le code suivant: https://codepen.io/Tronix117/pen/ZMPXdq

* Parcourir tous les éléments dont la classe est "item"

* Dès que le texte à l'intérieur correspond à "cible 1"
* Retirer la classe "light-grey" de l'item
* Ajouter la classe "pink" à cet item
* Incrémenter la valeur du compteur de l'élément HTML correspondant à la classe "target" de la cible 1

* Dès que le texte à l'intérieur correspond à "cible 2"
* Retirer la classe "light-grey" de l'item
* Ajouter la classe "red" à cet item
* Incrémenter la valeur du compteur de l'élément HTML correspondant à la classe "target" de la cible 2

# Exercice 2 
Recréer l’exercice 1 sans balise HTML : https://codepen.io/Tronix117/pen/wEOpMN

Toute la conception du DOM doit être réalisée en JavaScript, n'hésitez pas à le faire de manière propre et pratique en utilisant des tableaux et des boucles.

# Exercice 3
Récupérer ou Forkez le code suivant ; https://codepen.io/Tronix117/pen/jvJzzQ

Avec les événements et les manipulations des éléments, l'objectif est de réaliser un déplacement de DIV en drag & drop
* récupérer les éléments de classe draggableBox

Pour chacun d'entre eux mettre 2 écouteurs
* un mousedown qui va initier le déplacement
* un mouseup qui va stopper le déplacement

Globalement, mettre un écouteur sur mousemouve qui va permettre le suivi du drag & drop

