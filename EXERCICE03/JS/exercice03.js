/*--- --- ---   Exercice 3   --- --- ---*/
const DRAGGABLE_ELEMENTS_WIDTH = 100
const DRAGGABLE_ELEMENTS_HEIGHT = 100

let currentContentWidth = null
let currentContentHeight = null

document.addEventListener('DOMContentLoaded', () => {
   onResize() // to initialize currentContentWidth / currentContentHeight
   renderDraggableElements()
   attachDragEvents()
})

function attachDragEvents() {
   //-- Exercice principal : Implémentez le drag and drop
   //-- Exercice bonus 1 : la dernière box relachée doit être au dessus des autres
   //-- Exercice bonus 2 : lorsque deux box sont en contact, elles doivent être teintes en rouge
}

/*
const contentElement = document.getElementById('content')
    let currentDroppable = null;
    let drag = document.querySelectorAll(".draggableBox")
    console.log(drag)

    drag.forEach(item => {
        item.addEventListener('mousedown', e => {
            let shiftX = e.clientX - item.getBoundingClientRect().left
            let shiftY = e.clientY - item.getBoundingClientRect().top
            console.log(shiftX, shiftY)

            item.style.position = 'absolute';
            item.style.zIndex = 1000;
            contentElement.append(item);

            moveAt(e.pageX, e.pageY);

            function moveAt(pageX, pageY) {
                item.style.left = pageX - shiftX + 'px';
                item.style.top = pageY - shiftY + 'px';
            }

            function onMouseMove(event) {
                moveAt(event.pageX, event.pageY);

                item.hidden = true;
                let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
                item.hidden = false;

                if (!elemBelow) return;

                let droppableBelow = elemBelow.closest('.draggableBox');
                if (currentDroppable != droppableBelow) {
                    if (currentDroppable) { // null when we were not over a droppable before this event
                        item.style.background = ''
                        leaveDroppable(currentDroppable);
                    }
                    currentDroppable = droppableBelow;
                    if (currentDroppable) { // null if we're not coming over a droppable now
                        // (maybe just left the droppable)
                        item.style.background = 'red';
                        enterDroppable(currentDroppable);
                    }
                }
            }

            document.addEventListener('mousemove', onMouseMove);

            item.onmouseup = function () {
                document.removeEventListener('mousemove', onMouseMove);
                item.onmouseup = null;
            };
        })

        function enterDroppable(elem) {
            elem.style.background = 'red';
        }
        
        function leaveDroppable(elem) {
            elem.style.background = '';
        }

        item.ondragstart = function () {
            return false;
        };
    })
*/

function renderDraggableElements() {
   const contentElement = document.getElementById('content')
   const maxLeft = currentContentWidth - DRAGGABLE_ELEMENTS_WIDTH
   const maxTop = currentContentHeight - DRAGGABLE_ELEMENTS_HEIGHT
   
   for (let i = 0; i <= 10; i++) {
      const divElement = document.createElement('div')
      divElement.className = 'draggableBox'
      divElement.appendChild(document.createTextNode(`Box nº${i}`))
      divElement.style.left = Math.floor(Math.random() * maxLeft) + 'px'
      divElement.style.top = Math.floor(Math.random() * maxTop) + 'px'
      contentElement.appendChild(divElement)
   }
}

window.addEventListener('optimizedResize', onResize)

function onResize() {
   const contentElement = document.getElementById('content')
   
   //-- Exercice Bonus 3: implémenter ici le repositionnement des box lorsque la fenêtre change de taille, les box doivent proportionnellement se retrouver à la même place
   
   currentContentWidth = contentElement.offsetWidth
   currentContentHeight = contentElement.offsetHeight
}

// See https://developer.mozilla.org/en-US/docs/Web/Events/resize
// Prevent resize event to be fired way too often, this means neither lags nor freezes
{
   function throttle(type, name, obj = window) {
      let running = false
      const event = new CustomEvent(name)
      obj.addEventListener(type, () => {
         if (running) return
         running = true
         requestAnimationFrame(() => {
            obj.dispatchEvent(event)
            running = false
         })
      })
   }

   /* init - you can init any event */
   throttle('resize', 'optimizedResize');
}