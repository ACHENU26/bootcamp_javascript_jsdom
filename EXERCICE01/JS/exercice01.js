/*--- --- ---   Exercice 1   --- --- ---*/
document.addEventListener('DOMContentLoaded', () => {
    // -- your code here, after the DOM has been fully loaded

    var list = document.getElementsByClassName("item");
    let cptCible1 = 0
    let cptCible2 = 0

    for (let item of list) {
        console.log(item.childNodes[3]);

        if (item.childNodes[3].textContent == 'cible 1') {
            cptCible1++
            console.log(cptCible1)
            item.className = 'item pink'
            document.getElementsByClassName('target pink')[0].childNodes[3].textContent = cptCible1

        } else {
            cptCible2++
            console.log(cptCible2)
            item.className = 'item red'
            document.getElementsByClassName('target red')[0].childNodes[3].textContent = cptCible2
        }
    }
})
