/*--- --- ---   Exercice 2   --- --- ---*/
document.addEventListener('DOMContentLoaded', () => {
    // -- your code here, after the DOM has been fully loaded

    // MAIN DIV
    let content = document.createElement("div")
    content.id = 'content'

    // MENU DIV
    let menu = document.createElement("div")
    menu.className = "menu dark-grey"

    // TARGETS DIV 
    let target = document.createElement("div")
    target.className = "targets grey"

    for (let index = 1; index < 6; index++) {
        let c = document.createElement("div")
        c.className = "item light-grey"
        c.innerHTML = "<h2>" + index + "</h2><span>cible " + index + "</span>"
        menu.appendChild(c)
    }

    let t1 = document.createElement("div")
    t1.className = "target pink"
    let h3t = document.createElement("h3")
    h3t.innerHTML = "cible 1"

    let spanT = document.createElement("span")
    spanT.className = "compteur"
    spanT.innerHTML = 0

    t1.appendChild(h3t)
    t1.appendChild(spanT)

    let t2 = document.createElement("div")
    t2.className = "target red"
    let h3t2 = document.createElement("h3")
    h3t2.innerHTML = "cible 2"

    let spanT2 = document.createElement("span")
    spanT2.className = "compteur"
    spanT2.innerHTML = 0
    t2.appendChild(h3t2)
    t2.appendChild(spanT2)

    target.appendChild(t1)
    target.appendChild(t2)
    content.appendChild(menu)
    content.appendChild(target)
    document.body.appendChild(content)
})